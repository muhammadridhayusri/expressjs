const express = require('express')
const app = express()
const port = 4000
const controller = require('./controller/controller-1')
const middleWare = require('./middleware/middleware')
const routes = require('./route/index')

app.use(express.static('views/assets'))
app.set("view engine", "ejs")

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use(routes)
app.use(middleWare.errorHandler)

app.listen(port,()=> {
   console.log(`LISTENING ON PORT ${port}`)
})