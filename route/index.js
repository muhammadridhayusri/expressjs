const controller = require('../controller/controller-1')
const express = require('express')
const route = express()
const middleWare = require('../middleware/middleware')

route.use(express.json())

route.get('/chapter4', controller.chapter4)
route.get('/loginpage', controller.loginPage)
// route.get('/login', controller.loginGet)
route.post('/login', controller.loginPost)
route.get('/', middleWare.tesMiddleWare ,controller.homePage)
route.delete('/', controller.deleteHomePage)
route.put('/',controller.putHomePage)
route.post('/',controller.postHomePage)

route.all('*', (req, res) => {
   res.send(`<h2 style="color: darkred"> Page not Found </h2>`)
})

module.exports = route