const res = require('express/lib/response')
const people = require('./people')

class controller {
   static homePage(req,res,next)  {
      console.log('Home Page Open')
      try {
         res.render('bodych3')
      } catch(error) {
         console.log(error)
         next(error)
      }
   }

   static postHomePage(req, res, next) {
      try {
         res.status(200).send(`Home Page method Post`)
      } catch(error) {
         console.log(error)
         next(error)
      }
   }

   static deleteHomePage (req, res, next) {
      try {
         res.status(200).send(`Home Page method Delete`)
      } catch(error) {
         console.log(error)
         next(error)
      }
   }

   static putHomePage (req, res, next) {
      try {
         res.status(200).send(`Home Page method Put`)
      } catch(error) {
         console.log(error)
         next(error)
      }
   }

   static chapter4 (req,res,next) {
      console.log('Chapter 4 Opened')
      try {
         res.render('ch4')
      } catch(error) {
         console.log(error)
         next(error)
      }
   }

   static loginPage (req,res,next) {
      try {
         res.render('loginget')
      } catch(error) {
         console.log(error)
         next(error)
      }
   }



   static loginPost (req,res,next) {
      let _people = people.getUser()
      const {name} = req.body
      const {password} = req.body
      const userName = _people.find(people => people.name === name)
      if ( !userName) { 
         return res.status(400).send(`<h1 style="color: darkred;text-align: center">Wrong Username or Password ,${name}</h1>`)
      }
      const _userPassword = userName.password
      if (userName.name == name && password == _userPassword) {
         console.log(`login Sukses Method Post, Welcome ${name}`) 
         // for (let i = 0; i < _people.length; i++) {
         //    delete _people[i].password
         // }
         return res.status(200).json(_people)
      } else {
         return res.status(400).send(`<h1 style="color: darkred;text-align: center">Wrong Username or Password ,${name}</h1>`)
      }
   }

      // static loginGet (req,res,next) {
   //    // throw error
   //    const _people = people.getUser()
   //    const query = req.query.name
   //    const passWord = req.query.password 
   //    const userName = _people.find(people => people.name === query)    
   //    if (!userName) {   
   //       console.log('username not register')      
   //       return res.status(404).send(`<h1 style="text-align: center; color: darkred"> YOU ARE UNAUTHORIZED ${query} </h1>`)
   //    } 
   //    const _passWord = userName.password
   //    if (!userName || _passWord != passWord) {
   //       console.log('wrong password') 
   //       return res.status(404).send(`<h1 style="text-align: center; color: darkred">  ${query}, Wrong Password </h1>`)
   //    }
   //    console.log(`login Sukses Method Get, Welcome ${query}`) 
   //    res.status(200).redirect('/chapter4')

   //    // console.log(userName.name) 
   //    // res.status(200).json(_people.name)    
   //    // localStorage.setItem('name',JSON.stringify(query)) 
   //    // console.log(localStorage.getItem('name'))
   //    // console.log(query) 
   //    // console.log(_people)
   //    // // const {name} = req.body
   //    // // console.log(name)
   //    // console.log(req.query.name)
      
   //    }
}

module.exports = controller